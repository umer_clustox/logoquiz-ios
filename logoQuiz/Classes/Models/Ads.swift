//
//  Ads.swift
//  logoQuiz
//
//  Created by Apple PC on 10/8/18.
//  Copyright © 2018 Saira. All rights reserved.
//

import Foundation

public struct Ads: Decodable {
    
    let id: Int
    let appId: Int
    let adUrl: String?
    let appStoreUrl: String?
    let adType: String?
    
    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case appId = "app_id"
        case adUrl = "ad_url"
        case appStoreUrl = "app_url"
        case adType = "ad_type"
    }
    
}
