//
//  ResponseObject.swift
//  logoQuiz
//
//  Created by Apple PC on 10/8/18.
//  Copyright © 2018 Saira. All rights reserved.
//

import Foundation

struct ResponseObject: Decodable {
    
    let ads: [Ads]
    let app: AppObject
}

