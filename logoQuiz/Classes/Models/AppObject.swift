//
//  AppObject.swift
//  logoQuiz
//
//  Created by Apple PC on 25/8/18.
//  Copyright © 2018 Saira. All rights reserved.
//

import Foundation

public struct AppObject: Decodable {
    
    let id: Int
    let name: String?
    let frequency: Int?
    
    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case frequency = "frequency_of_app"
    }
    
}
