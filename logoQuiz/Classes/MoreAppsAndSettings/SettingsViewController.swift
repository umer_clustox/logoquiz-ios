//
//  SettingsViewController.swift
//  GuessTheRugbyPlayer
//
//  Created by Saira on 1/3/17.
//  Copyright © 2017 Saira. All rights reserved.
//

import UIKit

class SettingsViewController: GAITrackedViewController {
    
    @IBOutlet var soundSwitch: UISwitch!
    @IBOutlet weak var bannerView: BannerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        soundSwitch.setOn(!DataStorage.shared.soundOff, animated: true)
    }
    
    @IBAction func dismissButtonWasClicked(_ sender: AnyObject) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func soundSwitchValueChanged(_ sender: Any) {
        guard let tappedSwitch = sender as? UISwitch else {
            return
        }
        DataStorage.shared.soundOff = !tappedSwitch.isOn
    }
    
}

