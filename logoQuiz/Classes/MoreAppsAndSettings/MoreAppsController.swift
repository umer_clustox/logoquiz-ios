//
//  MoreAppsController.swift
//  GuessTheCity
//
//  Created by Saira on 12/3/15.
//  Copyright © 2015 Saira. All rights reserved.
//

import UIKit

class MoreAppsController: GAITrackedViewController, CAAnimationDelegate {
    
    @IBOutlet var scrlView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (DataStorage.shared.moreApps.count > 0) {
            self.updateMoreAppsVersion()
        }
    }
    
    @IBAction func dismissView(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    
    Next Update will cache MoreApps and use Version checks to update itself.
    Will also improve subscription i.e. once you're subscribed; you can unsubsribe or something.
    
    */
    func updateMoreAppsVersion() {
        autoreleasepool {
            /*
            Comment the following two lines
            and uncomment the next two lines to pick up
            a local copy of the MoreApps plist file.
            */
            
            let productName = (Bundle.main.infoDictionary! as NSDictionary).object(forKey: kCFBundleNameKey) as! String
            
            var y = 5 as CGFloat
            let allKeys = (DataStorage.shared.moreApps.allKeys as NSArray)
            for i in 0 ..< DataStorage.shared.moreApps.count {
                let key = allKeys.object(at: i) as! String
                if key != productName {
                    
                    var height: CGFloat = 103.0
                    let deviceIdiom = UIScreen.main.traitCollection.userInterfaceIdiom
                    switch (deviceIdiom) {
                    case .pad:
                        height = 150.0
                    default:
                        height = 103.0
                    }
                    
                    let dict = DataStorage.shared.moreApps.object(forKey: key) as! NSDictionary
                    let customView = CustomizedMoreAppsCellView(frame: CGRect(x: 0, y: y, width: self.view.bounds.size.width, height: height), maApp: dict)
                    customView.frame = CGRect(x: 0, y: y, width: self.view.bounds.size.width, height: height)
                    customView.isHidden = true
                    scrlView.addSubview(customView)
                    
                    if i % 2 == 0 {
                        customView.slideIn(from: kFTAnimationRight, duration: 0.4, delegate: nil)
                    }
                    else {
                        customView.slideIn(from: kFTAnimationLeft, duration: 0.4, delegate: nil)
                    }
                    
                    y = y + height + 10;
                }
            }
            
            scrlView.contentSize = CGSize(width: self.view.bounds.size.width, height: y)
        }
    }

    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        //var targetView = anim.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.screenName = "More Apps Screen";
    }

}
