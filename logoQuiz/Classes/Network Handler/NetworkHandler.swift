//
//  NetworkHandler.swift
//  iOSBasicArchitechture
//
//  Created by Saira Samdani on 1/24/18.
//  Copyright © 2018 Saira Samdani. All rights reserved.
//

import Foundation
import Alamofire

public struct NetworkHandler {
    
    let commonHeaders: HTTPHeaders = ["Content-Type":"application/json",
                                      "Accept":"application/json"]
    
    public typealias ResponseHandler = (_ response: DataResponse<Any>?, _ error: Error?) -> ()
    
    public func requestForPost(method: HTTPMethod,
                        headers: [String: String]? = nil,
                        params: [String: Any]? = nil ,
                        url: URL,
                        completion: ResponseHandler? = nil) {
        
        var authHeaders = commonHeaders
        
        if let headers = headers {
            authHeaders.merge(headers, uniquingKeysWith: { (first, _) in first })
        }
        
        Alamofire.request(url, method: method, parameters: params, encoding: JSONEncoding.default, headers: authHeaders).validate().responseJSON { response in
            
            switch response.result {
            case .success:
                completion?(response, nil)
            case .failure(let error):
                completion?(response, error)
            }
        }
        
    }
    
    public func requestForGET(headers: [String: String]? = nil,
                               params: [String: Any]? = nil ,
                               url: URL,
                               completion: ResponseHandler? = nil) {
        
        var authHeaders = commonHeaders
        
        if let headers = headers {
            authHeaders.merge(headers, uniquingKeysWith: { (first, _) in first })
        }
        
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: authHeaders).validate().responseJSON { response in
            
            switch response.result {
            case .success:
                completion?(response, nil)
            case .failure(let error):
                completion?(response, error)
            }
        }
    }
}
