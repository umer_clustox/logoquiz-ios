//
//  HomeScreenViewController.swift
//  GuessTheFlag
//
//  Created by Saira on 12/28/16.
//  Copyright © 2016 Saira. All rights reserved.
//

import UIKit
import Google
import Foundation
import NVActivityIndicatorView
import Kingfisher

class HomeScreenViewController: GAITrackedViewController, NVActivityIndicatorViewable {
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var levelsButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var bannerView: BannerView!
    @IBOutlet weak var adImage: UIImageView!
    @IBOutlet weak var adView: UIView!
    let shareController = ShareController()
    let networkHandler = NetworkHandler()
    
    let decoder = JSONDecoder()
    
    var adsArray = [Ads]()
    
    var adsImagesCount = 0
    var turnCount = 0
    var adsImagesArray = [Image]()
    
    var isPlayingForTheFirstTime = true
    
    var ad: Ads?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(HomeScreenViewController.imageTapped(gesture:)))
        
        self.adImage.addGestureRecognizer(tapGesture)
        self.adImage.isUserInteractionEnabled = true
        
        if self.isPlayingForTheFirstTime {
            self.getAds()
            self.isPlayingForTheFirstTime = false
        }

        facebookButton.configureForFacebook()
        self.enablePlayButtonsWhenQuizIsReady()
        
        shareController.delegate = self
        
        self.decoder.dateDecodingStrategy = .secondsSince1970
    }
    
    func imageTapped(gesture: UIGestureRecognizer) {
        
        if let _ = gesture.view as? UIImageView {
            print("Image Tapped")
            
            if let adObj = self.ad,
                var urlString = adObj.appStoreUrl {
                urlString = urlString.replacingOccurrences(of: "https://", with: "itms-apps://")
                
                guard let url = URL(string: urlString) else {
                    return
                }
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    func getAds() {
        let urlString = Constants.adAppUrl.appending(Constants.adAppId)
        if let url = URL(string: urlString) {
            networkHandler.requestForGET(headers: nil, params: nil, url: url) {
                response, error in
                
                if let data = response?.data {
                    if let error = error {
                        print(error)
                    } else {
                        do {
                            let repo = try self.decoder.decode(ResponseObject.self, from: data)
                            let ads = repo.ads
                            self.adsArray.removeAll()
                            self.adsArray.append(contentsOf: ads)
                            self.downloadImages()
                            Constants.ads_repeat_frequency = repo.app.frequency ?? 5
                        } catch (let error){
                            print(error)
                        }
                    }
                } else {
                    print(error)
                }
            }
        }
    }
    
    @IBAction func startButtonWasPressed(_ sender: UIButton) {
        self.turnCount = self.turnCount + 1
        if self.adsImagesArray.count > 0,
            self.turnCount % Constants.ads_repeat_frequency == 1 {
            if self.adsImagesArray.count > self.adsImagesCount {
                self.adImage.image = self.adsImagesArray[self.adsImagesCount]
                self.ad = self.adsArray[self.adsImagesCount]
                self.adsImagesCount = self.adsImagesCount + 1
            } else {
                self.adsImagesCount = 0
                self.adImage.image = self.adsImagesArray[self.adsImagesCount]
            }
            self.adView.fadeIn()
        } else {
            self.performSegue(withIdentifier: "SegueGamePlayViewController", sender: nil)
        }
        
//        if self.adsArray.count > 0 {
//            if self.adsArray.count > self.adsImagesCount {
//                let ad = self.adsArray[self.adsImagesCount]
//                self.adsImagesCount = self.adsImagesCount + 1
//                if let urlString = ad.adUrl,
//                    let url = URL(string: urlString) {
//                    self.adImage.kf.setImage(with: url)
//                }
//            } else {
//                self.adsImagesCount = 0
//                let ad = self.adsArray[self.adsImagesCount]
//                self.adsImagesCount = self.adsImagesCount + 1
//                if let urlString = ad.adUrl,
//                    let url = URL(string: urlString) {
//                    self.adImage.kf.setImage(with: url)
//                }
//            }
//        } else {
//            self.performSegue(withIdentifier: "SegueGamePlayViewController", sender: nil)
//        }
        
//        if let urlString = self.adsArray[0].adUrl,
//            let url = URL(string: urlString) {
//            self.adImage.kf.setImage(with: url)
//            self.adView.popIn()
//        }
    }
    
    @IBAction func facebookButtonWasPressed(_ sender: Any) {
        shareController.share(onFacebook: Constants.iTunesURL,
                              description: "")
    }
    
    @IBAction func cancelButtonWasPressed(_ sender: UIButton) {
        
        self.adView.fadeOut()
        self.performSegue(withIdentifier: "SegueGamePlayViewController", sender: nil)
    }
    
    func enablePlayButtonsWhenQuizIsReady() {
        DispatchQueue.main.async {
            if (self.startButton != nil) {
                if DataStorage.shared.quizDict.count > 0 {
                    self.stopAnimating()
//                    self.startButton.isEnabled = true
//                    self.levelsButton.isEnabled = true
                } else {
//                    self.startButton.isEnabled = false
//                    self.levelsButton.isEnabled = false
                }
            }
        }
    }
    
    func downloadImages() {
        self.adsImagesArray.removeAll()
        self.adsImagesCount = 0
        for ad in self.adsArray {
            if let urlString = ad.adUrl,
                let url = URL(string: urlString) {
                ImageDownloader.default.downloadImage(with: url, options: [], progressBlock: nil) {
                    (image, error, url, data) in
                    if let image = image {
                        self.adsImagesArray.append(image)
                    }
                }
            }
        }
    }
    
    // MARK: UIViewController Methods
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "ShowGamePlaySegue" {
            guard let controller = segue.destination as? GamePlayViewController else {
                    return
            }
            controller.forLevel = false
            DataStorage.shared.level = 1
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.screenName = "Home Screen";
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

// MARK: ShareControllerDelegate Conformance
extension HomeScreenViewController: ShareControllerDelegate {
    internal func errorReceived(message: String) {
        self.showAlert("Error", message: message, okTitle: "OK", delegate: self)
    }
}
