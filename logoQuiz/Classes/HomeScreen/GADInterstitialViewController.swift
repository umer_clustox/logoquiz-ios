//
//  GADInterstitialViewController.swift
//  GuessTheRugbyPlayer
//
//  Created by Saira on 12/30/16.
//  Copyright © 2016 Saira. All rights reserved.
//

import Foundation
import Google

protocol GADInterstitialControllerDelegate: class {
    func interstitialDidReceiveAd(_ ad: GADInterstitial);
    func interstitialDidDismissScreen(_ ad: GADInterstitial);
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError);
}

public class GADInterstitialViewController: NSObject {
    // Delegate
    weak var delegate: GADInterstitialControllerDelegate?
        
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitialTemp = GADInterstitial.init(adUnitID: Constants.admobInterstitialID)
        interstitialTemp.delegate = self;
        interstitialTemp.load(GADRequest())
        return interstitialTemp;
    }
    
}

// MARK: GADInterstitialDelegate Conformance
extension GADInterstitialViewController: GADInterstitialDelegate {
    public func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        self.delegate?.interstitialDidReceiveAd(ad)
    }
    
    public func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        self.delegate?.interstitial(ad, didFailToReceiveAdWithError: error)
    }
    
    public func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        self.delegate?.interstitialDidDismissScreen(ad)
    }
}
