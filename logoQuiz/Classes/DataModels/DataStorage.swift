//
//  DataStorage.swift
//  GuessTheRugbyPlayer
//
//  Created by Saira on 12/29/16.
//  Copyright © 2016 Saira. All rights reserved.
//

import Foundation
import UIKit

class DataStorage: NSObject {
    
    let user = User()
    static let shared = DataStorage()
    
    var quizDict = [Int: [Question]]()
    var moreApps = NSMutableDictionary()
    var maxNo: Int = -1
    var currentQs: Int = -1
    var version = 1.0
    var level = 0
    var getRandom = false
    var soundOff = false
    
    fileprivate override init() {
        super.init()
    }
    
    func loadFromDisk() {
        let theHighScore = UserDefaults.standard.integer(forKey: "totalScore")
        if theHighScore > 0 {
            self.user.bestScore = theHighScore
        }
        
        let theLastLevel = UserDefaults.standard.integer(forKey: "level")
        if theLastLevel > 0 {
            self.user.currentLevel = theLastLevel
        }
        
        self.soundOff = UserDefaults.standard.bool(forKey: "soundOff")
        self.performSelector(inBackground: #selector(DataStorage.loadQuizAndMoreApps), with: nil)
    }
    
    func loadQuizAndMoreApps() {
        if self.quizDict.count <= 0 {
            self.quizDict = DatabaseHandler.fetch(questions: "logo_quiz", querySql: "SELECT * FROM quiz")
            movetoHomeScreen()
        }
        
        self.updateMoreApps()
    }
    
    func movetoHomeScreen() {
        guard let navigationController = UIApplication.appDelegate?.window?.rootViewController as? UINavigationController,
            let homeViewController = navigationController.viewControllers[0] as? HomeScreenViewController else {
                return
        }
        homeViewController.enablePlayButtonsWhenQuizIsReady()
    }
    
    func saveToDisk() {
        UserDefaults.standard.set(self.soundOff, forKey: "soundOff")
        UserDefaults.standard.set(NSNumber(value: self.user.bestScore as Int), forKey: "totalScore")
        UserDefaults.standard.set(NSNumber(value: self.user.currentLevel as Int), forKey: "level")
    }
    
    //MARK: Load More Apps
    func updateMoreApps() {
        autoreleasepool {
            let thePath = Bundle.main.path(forResource: "MoreApps", ofType: "plist")
            if thePath != nil {
                let tempDict = NSMutableDictionary(contentsOfFile: thePath!)
                if tempDict != nil {
                    if (UserDefaults.standard.object(forKey: "version") != nil) {
                        self.version = ((UserDefaults.standard.object(forKey: "version") as AnyObject).doubleValue)!
                    }
                    self.moreApps.addEntries(from: tempDict?.object(forKey: "MoreApps") as! [AnyHashable: Any])
                }
            }
        }
    }
    
    //MARK: Reset Values
    
    func resetValues(forLevel: Bool) {
        maxNo = 0;
        self.level = 0;
        self.currentQs = -1;
        self.getRandom = false
        
        guard let navigationController = UIApplication.appDelegate?.window?.rootViewController as? UINavigationController,
            let homeViewController = navigationController.viewControllers[0] as? HomeScreenViewController else {
                return
        }
        homeViewController.enablePlayButtonsWhenQuizIsReady()
        self.fillQuizData()
    }
    
    func fillQuizData() {
        self.quizDict.removeAll()
        self.quizDict = DatabaseHandler.fetch(questions: "logo_quiz", querySql: "SELECT * FROM quiz")
        movetoHomeScreen()
    }
    
    //MARK: GameEnd
    func checkForGameEnd(forLevel: Bool) -> Bool {
        if forLevel {
            let array = self.quizDict[self.level]
            if self.currentQs == array!.count - 1 {
                return true
            }
            return false
        } else {
            if self.quizDict.count == 0 {
                guard let navigationController = UIApplication.appDelegate?.window?.rootViewController as? UINavigationController,
                    let homeViewController = navigationController.viewControllers[0] as? HomeScreenViewController else {
                        return true
                }
                homeViewController.enablePlayButtonsWhenQuizIsReady()
                return true
            }
            return false
        }
    }
    
    //MARK: Next Question
    func calculateNextQs(forLevel: Bool) {
        if forLevel {
            self.currentQs = self.currentQs + 1
        } else {
            if getRandom {
                self.randomLevelRandomQs()
            }
            else {
                //If you have not shown max limit question i.e., 10 from a particular level. Keep showing more
                if (maxNo > 0) {
                    self.randomQuestion()
                    maxNo -= 1;
                }
                else {
                    //self.level += 1;
                    //check if you have shown question from all 4 levels. Now start showing random
                    let allKeys = Array(self.quizDict.keys)
                    if (self.level > self.quizDict.count || !(allKeys.contains(self.level))) {
                        getRandom = true;
                        self.randomLevelRandomQs()
                    }
                    else {
                        maxNo = 9;
                        let array = self.quizDict[self.level]
                        if ((array! as AnyObject).count < 10) {
                            maxNo = (array! as AnyObject).count - 1;
                        }
                        self.randomQuestion()
                    }
                }
            }
        }
    }
    
    func randomLevelRandomQs() {
        let allKeys = Array(self.quizDict.keys)
        let sortedArray = allKeys.sorted()
        if sortedArray.count > 1 {
            let number = self.randomNumberBetweenTwoNumbers(0, largest: UInt32(sortedArray.count-1))
            self.level = allKeys[number]
        }
        else {
            self.level = sortedArray.last!
        }
        
        self.randomQuestion()
    }
    
    func randomQuestion() {
        let array = self.quizDict[self.level]
        if array?.count == 1 {
            self.currentQs = 0
        }
        else {
            self.currentQs = self.randomNumberBetweenTwoNumbers(0, largest: UInt32(array!.count-1))
        }
    }
    
    func randomNumberBetweenTwoNumbers(_ smallest: UInt32, largest: UInt32) -> Int {
        let randomNumber = arc4random_uniform(largest - smallest) + smallest
        return Int(randomNumber);
    }
    
    func deleteAnsweredQuestion(forLevel: Bool) {
        if !forLevel {
            if self.currentQs >= 0 {
                //To avoid repeating questions, remove answered question from main quiz dict
                //and add to another structure answeredQsDict,
                //at end of game move questions from answeredQsDict back to quizDict.
                var array = self.quizDict[self.level]
                if (array != nil && self.currentQs <= array!.count-1) {
                    array!.remove(at: self.currentQs)
                    self.quizDict[self.level] = array
                    if array!.count == 0 {
                        self.quizDict.removeValue(forKey: self.level)
                    }
                }
            }
        }
    }
}
