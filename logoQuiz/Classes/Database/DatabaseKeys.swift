//
//  DatabaseKeys.swift
//  POIBase
//
//  Created by Saira Samdani on 9/27/17.
//  Copyright © 2017 Saira Samdani. All rights reserved.
//

import Foundation

/**
 *  Struct for declaring this database global tables and columns names
 */
struct DBKeys {
    static let COL_QUESTION_TEXT = "questionText"
    static let COL_ANSWER = "answer"
    static let COL_OPTION_1 = "option1"
    static let COL_OPTION_2 = "option2"
    static let COL_OPTION_3 = "option3"
    static let COL_IMAGE_ID = "imageId"
    static let COL_LEVEL = "level"
    
    static let TABLE_NAME = "quiz"
    static let dbName: String = "logo_quiz"
}
