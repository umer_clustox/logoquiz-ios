//
//  DatabaseHandler.swift
//  POIBase
//
//  Created by Saira Samdani on 9/21/17.
//  Copyright © 2017 Saira Samdani. All rights reserved.
//

import Foundation
import SQLite3

class DatabaseHandler {
    
    weak var delegate: DataParserDelegate?
    
    static func loadFrom(Bundle fileName: String) -> NSURL? {
        guard let sourceSqliteURL = Bundle.main.url(forResource: fileName, withExtension: "db") else {
            return nil
        }
        
        return sourceSqliteURL as NSURL
    }
    
    static func fetch(questions databaseName: String, querySql: String) -> [Int : [Question]] {
        
        // Declare an array to hold full resultset
        var resultSet = [Question]()
        var quizDict = [Int: [Question]]()
        var db: OpaquePointer?
    
        guard let documentPath = DatabaseHandler.loadFrom(Bundle: databaseName) else {
            let error = SQLiteError.PathNotFound
            //debugPrint("\(error)")
            return quizDict
        }
        
        if sqlite3_open(documentPath.path, &db) == SQLITE_OK {
            var statement: OpaquePointer?
            let code = sqlite3_prepare_v2(db, querySql, -1, &statement, nil)
            guard code  == SQLITE_OK else {
                let error = DatabaseHandler.getError(db: db)
                //debugPrint("\(error)")
                return quizDict
            }
            
            defer {
                if statement != nil {
                    sqlite3_finalize(statement)
                }
                if db != nil {
                    sqlite3_close(db)
                }
            }
            
            while(sqlite3_step(statement) == SQLITE_ROW) {
                // Declare an array to keep the data for each fetched row.
                var rowData = [String: String]()
                
                // Get the total number of columns.
                let totalColumns = sqlite3_column_count(statement);
                
                // Go through all columns and fetch each column data.
                for i in 0 ... totalColumns {
                    // Convert the column data to text (characters).
                    let dbDataAsChars = sqlite3_column_text(statement, i)
                    let colNameAsChars = sqlite3_column_name(statement, Int32(i))
                    // If there are contents in the currenct column (field) then add them to the current row array.
                    if dbDataAsChars != nil {
                        // Convert the characters to string.
                        let value = String(cString: UnsafePointer<UInt8>(dbDataAsChars!))
                        let key = String.init(cString: UnsafePointer<CChar>(colNameAsChars!))
                        rowData[key] = value
                    }
                }
                
                // Store each fetched data row in the results array, but first check if there is actually data.
                if rowData.count > 0 {
                    let question = Question(optionOne: rowData[DBKeys.COL_OPTION_1] ?? "",
                                            optionTwo: rowData[DBKeys.COL_OPTION_2]!,
                                            optionThree: rowData[DBKeys.COL_OPTION_3] ?? "",
                                          answer: rowData[DBKeys.COL_ANSWER] ?? "",
                                          level: Int(rowData[DBKeys.COL_LEVEL]!)!)
                    resultSet.append(question)
                    let keys = Array(quizDict.keys)
                    if keys.contains(question.level) {
                        var array = quizDict[question.level]
                        array?.append(question)
                        quizDict[question.level] = array
                    }
                    else {
                        var array = [Question]()
                        array.append(question)
                        quizDict[question.level] = array
                    }
                }
            }
        } else {
            defer {
                if db != nil {
                    sqlite3_close(db)
                }
            }
            let error = DatabaseHandler.getError(db: db)
            //debugPrint("\(error)")
        }
    
        return quizDict
    }
    
    private static func getError(db: OpaquePointer?) -> String {
        let error = String(cString: sqlite3_errmsg(db))
        if !error.isEmpty {
            return error
        } else {
            return "No error message provided from sqlite."
        }
    }
    
}

enum SQLiteError: Error {
    case PathNotFound
    case DatabaseNotFound
    case OpenDatabase(message: String)
    case Prepare(message: String)
    case Step(message: String)
    case Bind(message: String)
}
