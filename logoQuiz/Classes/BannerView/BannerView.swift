//
//  BannerView.swift
//  GuessTheRugbyPlayer
//
//  Created by Saira on 12/29/16.
//  Copyright © 2016 Saira. All rights reserved.
//

import UIKit

class BannerView : UIView, GADBannerViewDelegate {
    var gAdBannerView: GADBannerView!
    var bannerIsVisible: Bool
    var isGadBannerLoaded: Bool
    
    required init?(coder aDecoder: NSCoder) {
        isGadBannerLoaded = false
        bannerIsVisible = false
        super.init(coder: aDecoder)
        
        self.initgAdBanner()
        let screenBounds : CGRect = UIScreen.main.bounds
        let rect : CGRect = CGRect(x: 0, y: 0, width: screenBounds.width, height: self.frame.size.height)
        self.gAdBannerView.frame = rect
        self.addSubview(self.gAdBannerView)
        
        self.backgroundColor = UIColor.clear
    }
    
    override init(frame: CGRect) {
        isGadBannerLoaded = false
        bannerIsVisible = false
        super.init(frame: frame)
        
        self.initgAdBanner()
        let screenBounds : CGRect = UIScreen.main.bounds
        let rect : CGRect = CGRect(x: 0, y: 0, width: screenBounds.width, height: self.frame.size.height)
        self.gAdBannerView.frame = rect
        self.addSubview(self.gAdBannerView)
        
        self.backgroundColor = UIColor.clear
    }
    
    func initgAdBanner() {
        if self.gAdBannerView == nil {
            self.gAdBannerView = GADBannerView()
            self.gAdBannerView.adUnitID = Constants.admobBannerID
            self.gAdBannerView.adSize = kGADAdSizeSmartBannerPortrait
            self.gAdBannerView.delegate = self
            
            let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
            self.gAdBannerView.rootViewController = appDelegate.window?.rootViewController
        }
        
        isGadBannerLoaded = false
        self.gAdBannerView.load(GADRequest())
    }
    
    // MARK: GADBannerViewDelegate Methods
    
    // Called before ad is shown, good time to show the add
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.25)
        UIView.setAnimationCurve(UIViewAnimationCurve.easeInOut)
        
        self.gAdBannerView.isHidden = false;
        UIView.commitAnimations()
        
        isGadBannerLoaded = true;
        self.bringSubview(toFront: self.gAdBannerView)
    }
    
    // An error occured
    
    func didFailToReceiveAdWithError(_ adView:GADBannerView,  error: GADRequestError) {
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.25)
        UIView.setAnimationCurve(UIViewAnimationCurve.easeInOut)
        
        self.gAdBannerView.isHidden = true;
        UIView.commitAnimations()
        
        if (isGadBannerLoaded) {
            bannerIsVisible = false;
        }
        
        isGadBannerLoaded = false;
    }
    
}
