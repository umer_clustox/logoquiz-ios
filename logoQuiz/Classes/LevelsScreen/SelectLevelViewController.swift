//
//  SelectLevelViewController.swift
//  GuessTheRugbyPlayer
//
//  Created by Saira on 12/29/16.
//  Copyright © 2016 Saira. All rights reserved.
//

import UIKit

class SelectLevelViewController: GAITrackedViewController {
    @IBOutlet weak var bannerView: BannerView!
    @IBOutlet weak var tableView: UITableView!
    let levels = Array(DataStorage.shared.quizDict.keys).sorted()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
    }

    @IBAction func dismissButtonWasPressed(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    // MARK: UIViewController Methods
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "ShowGamePlayeSegue" {
            guard let controller = segue.destination as? GamePlayViewController else {
                return
            }
            controller.forLevel = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.screenName = "Home Screen"
        self.tableView.reloadData()
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension SelectLevelViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return levels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "level_cell") else {
            fatalError("No cell with this identifier")
        }
        
        let level = levels[indexPath.row]
        guard let button = cell.contentView.viewWithTag(1) as? UIButton else {
            fatalError("No button added to cell")
        }
        
        button.setTitle("Level \(level)", for: .normal)
        if level <= DataStorage.shared.user.currentLevel {
            button.isEnabled = true
        } else {
            button.isEnabled = false
        }
        
        return cell
    }
}

extension SelectLevelViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let level = levels[indexPath.row]
        if level <= DataStorage.shared.user.currentLevel {
            DataStorage.shared.level = level
            self.performSegue(withIdentifier: "ShowGamePlayeSegue", sender: tableView)
        }
    }
}
