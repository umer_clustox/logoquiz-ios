//
//  GuessTheRugbyPlayer-Bridging-Header.h
//  GuessTheRugbyPlayer
//
//  Created by Saira on 12/29/16.
//  Copyright © 2016 Saira. All rights reserved.
//

#ifndef GuessTheRugbyPlayer_Bridging_Header_h
#define GuessTheRugbyPlayer_Bridging_Header_h

#import <Google/Analytics.h>
#import <GoogleAnalytics/GAILogger.h>
#import "GAI.h"
#import "FTAnimation.h"

#import <GoogleMobileAds/GADBannerViewDelegate.h>
#import <GoogleMobileAds/GADInterstitialDelegate.h>
#import <GoogleMobileAds/GADBannerView.h>
#import <GoogleMobileAds/GADInterstitial.h>

#import "DALabeledCircularProgressView.h"

#endif /* GuessTheRugbyPlayer_Bridging_Header_h */
