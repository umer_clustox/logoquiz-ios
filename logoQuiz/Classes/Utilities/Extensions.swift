//
//  Extensions.swift
//  GuessTheRugbyPlayer
//
//  Created by Saira on 12/29/16.
//  Copyright © 2016 Saira. All rights reserved.
//

import Foundation
import UIKit

extension NSObject {
    
    func showAlert(_ title: String, message: String, okTitle: String, delegate: AnyObject) {
        let alertController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler :{ (action: UIAlertAction!) -> Void in
        })
        let okAction: UIAlertAction = UIAlertAction(title: okTitle, style: UIAlertActionStyle.default, handler :{ (action: UIAlertAction!) -> Void in
        })
        
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        
        if delegate is UIViewController {
            delegate.present(alertController, animated:true, completion: nil)
        }
    }
    
    func connected() -> Bool {
        var isAvailable  = true;
        guard let reachability = Reachability() else {
            return false
        }
        
        if !reachability.isReachable {
            isAvailable = false
        }
        
        return isAvailable;
        
    }
    
    func printFontNames() {
        let fontFamilies = UIFont.familyNames
        for fontFamily in fontFamilies {
            let fontNames: Array = UIFont.fontNames(forFamilyName: fontFamily)
            print("%@: %@", fontFamily, fontNames);
        }
    }
}

extension UIButton {
    func addPadding(left: CGFloat,
                    right: CGFloat) {
        self.contentEdgeInsets = UIEdgeInsetsMake(0, left, 0, right)

    }
    
    func configureForFacebook() {
        let deviceIdiom = UIScreen.main.traitCollection.userInterfaceIdiom
        
        // 2. check the idiom
        switch (deviceIdiom) {
            
        case .pad:
            self.addPadding(left: 80, right: 0)
        case .phone:
            self.addPadding(left: 30, right: 0)
        case .tv:
            self.addPadding(left: 30, right: 0)
        default:
            self.addPadding(left: 30, right: 0)
        }
    }
}

extension UILabel {
    func configure() {
        self.layer.shadowOpacity = 1.0;
        self.layer.shadowRadius = 3.0;
        self.layer.masksToBounds = false;
        self.layer.shadowColor = UIColor.white.cgColor;
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0);
    }
}

extension UIImageView {
    func configure() {
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 3.0
    }
}

struct ScreenSize {
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType {
    static let IS_IPHONE_5          = ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6_7          = ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P_7p         = ScreenSize.SCREEN_MAX_LENGTH == 736.0
}
