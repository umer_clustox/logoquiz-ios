//
//  Colors.swift
//  GuessTheRugbyPlayer
//
//  Created by Saira on 12/29/16.
//  Copyright © 2016 Saira. All rights reserved.
//

import UIKit

/**
 Extension for defining Project's specific colors
 */
public extension UIColor {

    /**
     Class function for returning grey color of border of buttons
     - returns: Returns a UIColor object initialized with grey color.
     */
    public class func grey() -> UIColor {
        return UIColor(red: 161.0 / 255.0, green: 138.0 / 255.0, blue: 140.0 / 255.0, alpha: 0.5)
    }
}
