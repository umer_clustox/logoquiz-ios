//
//  Constants.swift
//  GuessTheRugbyPlayer
//
//  Created by Saira on 12/29/16.
//  Copyright © 2016 Saira. All rights reserved.
//

import Foundation

/**
 *  Struct for declaring this project's global values
 */
struct Constants {
    // Banner id for Admob
    static let admobBannerID = "ca-app-pub-8793358717797604/5633435886"
    
    // Interstitial Id for Admob
    static let admobInterstitialID = "ca-app-pub-8793358717797604/2540368686"
    
    // Admob Application ID
    static let googleApplicationID = "ca-app-pub-8793358717797604~4156702686"
    
    // Google Analytics ID
    static let googleAnalyticsID = "UA-55757458-5"
    
    // Appstore ID of this App
    static let appID = "955814187"
    
    // iTunes URL of App
    static let iTunesURL = "http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=\(appID)&mt=8&uo=6"
    
    // URL hosting more apps file
    static let moreAppsFileURL = ""
    
    // App Id of app in URL
    static let adAppId = "45"
    
    // URL of ad app
    static let adAppUrl = "http://cricrealtime.com/api/v1/app_ads?app_id="
    
    // Ads repetition frequency
    static var ads_repeat_frequency = 5
}
